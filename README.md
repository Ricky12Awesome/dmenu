dmenu - dynamic menu
====================
dmenu is an efficient dynamic menu for X.

#### This is my own build of dmenu, all patches used are located in `./patches`
you can configure this to your own liking by editing `config.def.h`

Patches
-------

* [case-insensitive](https://tools.suckless.org/dmenu/patches/case-insensitive/) - 
case insensitive search
* [center](https://tools.suckless.org/dmenu/patches/center/) - 
center dmenu using `-c` (also works with `-z` to set width)
* [fuzzymatch](https://tools.suckless.org/dmenu/patches/fuzzymatch/) - 
fuzzy search
* [fuzzyhighlight](https://tools.suckless.org/dmenu/patches/fuzzyhighlight/) - 
highlight fuzzy search
* [grid](https://tools.suckless.org/dmenu/patches/grid/) - 
show options in a grid layout using `-g`
* [gridnav](https://tools.suckless.org/dmenu/patches/gridnav/) - 
move left to right in grid layout
* [line hight](https://tools.suckless.org/dmenu/patches/line-height/) - 
change line height using `-h`
* [mouse support](https://tools.suckless.org/dmenu/patches/mouse-support/) - 
lets you use your mouse to select options
* [xyw](https://tools.suckless.org/dmenu/patches/xyw/) - 
lets you set the position of dmenu using `-x` and `-y` and `-z` to set width
* [alpha](https://dwm.suckless.org/patches/alpha/) - 
lets you have transparency, this took the most effort to get working, 
since dmenu doesn't have an alpha patch, so I compared mine to 
[Baitinq/dmenu.c](https://github.com/Baitinq/dmenu/blob/master/dmenu.c) 
plus the patch and made it work

Requirements
------------
In order to build dmenu you need the Xlib header files.


Installation
------------

```sh
git clone https://gitlab.com/Ricky12Awesome/dmenu
cd dmenu
./install.sh
```

Edit config.mk to match your local setup (dmenu is installed into
the /usr/local namespace by default).

Running dmenu
-------------
See the man page for details.
